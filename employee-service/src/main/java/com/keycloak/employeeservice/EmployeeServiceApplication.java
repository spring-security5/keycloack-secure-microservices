package com.keycloak.employeeservice;

import com.keycloak.employeeservice.config.SecurityContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Set;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class,
		UserDetailsServiceAutoConfiguration.class})
public class EmployeeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeServiceApplication.class, args);
	}

	@RestController
	@RequestMapping(path = "/api/employees")
	public static class EmployeeRestController {

		@Autowired
		private EmployeeService employeeService;

		@GetMapping
		@PreAuthorize("hasRole('TEST') && hasAuthority('view_tasks')")
		public String getEmployeeAndDepartment() {
			return employeeService.getEmployeeAndDepartment();
		}

		@GetMapping(path = "/roles")
		public ResponseEntity<Set<String>> getAuthorizedUserRoles() {
			return ResponseEntity.ok(SecurityContextUtils.getUserRoles());
		}
	}

	@Service
	public static class EmployeeService {

		@Autowired
		private DepartmentRestClient departmentRestClient;

		public String getEmployeeAndDepartment() {
			String employeeName = SecurityContextUtils.getUserName();
			String departmentName = departmentRestClient.getDepartmentName();

			return "Employee Service Returned: " + employeeName + ", \nDepartment Service Returned: " + departmentName;
		}
	}

	@Component
	public static class DepartmentRestClient {

		@Autowired
		private OAuth2RestTemplate oAuth2RestTemplate;

		public String getDepartmentName() {
			return oAuth2RestTemplate.getForObject("http://localhost:8095/api/departments/1", String.class);
		}
	}
}